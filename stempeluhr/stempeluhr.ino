#include "SSD1306.h" 
#include <MFRC522.h>
#include <DS3231.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>

#include "FS.h"


const char *ssid = "Zeit";
const char *password = "Zeit12345";

#define RST_PIN  3  
#define SS_PIN  15 

MFRC522 mfrc522(SS_PIN, RST_PIN); 
ESP8266WebServer server(80);
SSD1306  display(0x3c, D1, D2);
DS3231 rtcClock;

int secondOld = 0;
 
void handleRoot() 
{
  File f = SPIFFS.open("data.txt", "r");
  if (f) 
  {
    server.streamFile(f, "text/plain");
    f.close();
  }
  else 
  {
    server.send(200, "text/html", "Keine Daten !!");
  }
}

void deleteSd() 
{
   SPIFFS.remove("data.txt");
   server.send(200, "text/html", "Sauber !!");
}

void formatSd() 
{
   SPIFFS.format();
   server.send(200, "text/html", "Formatiert !!");
}

void setTheTime() 
{
  String clk = server.arg("clk");
  DateTime dt(clk.toInt());

  rtcClock.setSecond(dt.second()); 
  rtcClock.setMinute(dt.minute()); 
  rtcClock.setHour(dt.hour()); 
  rtcClock.setDate(dt.day()); 
  rtcClock.setMonth(dt.month()); 
  rtcClock.setYear(dt.year()-2000); 
  rtcClock.setClockMode(false); 

  server.send(200, "text/html", "Uhrzeit gesetzt !!");
}

String path = server.arg("dir");

void setup() 
{
  delay(1000);

  pinMode(D0, OUTPUT); // Beeper
  digitalWrite(D0, HIGH);   

  pinMode(D3, INPUT_PULLUP); // Kommen
  //digitalWrite(D3, HIGH);   

  pinMode(D4, INPUT_PULLUP); // Gehen
  //digitalWrite(D4, HIGH);   
  
  Serial.begin(9600);

  display.init(); 
  display.flipScreenVertically();               

  SPI.begin();           
  mfrc522.PCD_Init();    
  
  SPIFFS.begin();

  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();

  server.on("/", handleRoot); 
  server.on("/delete", deleteSd);  
  server.on("/format", formatSd);  
  server.on("/time", setTheTime);  
  
  server.begin();
  Serial.println("HTTP Server gestartet");
}

String getUid()
{
  String uid;
  uid.reserve(20);

  byte *buffer = mfrc522.uid.uidByte;
  byte bufferSize = mfrc522.uid.size;

  for (byte i = 0; i < bufferSize; i++) 
  {
    uid += (buffer[i] < 0x10 ? "0" : "");
    uid += String(buffer[i], HEX);
  }
  return uid;
}


void loop() { 

  server.handleClient();

  digitalClockDisplay();
  
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    delay(50);
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    delay(50);
    return;
  }

  Serial.print(F("Card UID:"));
  Serial.println(getUid());

  digitalWrite(D0, LOW);       
  delay(100);
  digitalWrite(D0, HIGH); 

  display.setFont(ArialMT_Plain_24);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.clear();
  display.drawString(64, 5, "< Kommen");  
  display.drawString(64, 32, "Gehen >");  
  display.display();

  bool kommen = true;
  int counter = 0;

  while( true )
  {
    if (digitalRead(D3) == 0)
    {
      kommen = true;
      break;
    }
    else if (digitalRead(D4) == 0)
    {
      kommen = false;
      break;
    }
    delay(100);

    counter++;

    if (counter > 100)
    {
      digitalWrite(D0, LOW);       
      delay(1000);
      digitalWrite(D0, HIGH); 
      return;
    }
  }

  display.setFont(ArialMT_Plain_24);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.clear();
  if (kommen)
  {
    display.drawString(64, 22, "Hallo");  
  }
  else
  {
    display.drawString(64, 22, "Tschüss");  
  }
  display.display();

  File f = SPIFFS.open("data.txt", "a");
  if (f) 
  {

    DateTime dt = RTClib::now();
    String dateStr = (String)dt.year() + '-' + twoDig(dt.month()) + '-' + twoDig(dt.day());
    String timeStr = twoDig(dt.hour()) + ':' + twoDig(dt.minute()) + ':' + twoDig(dt.second());

    if (kommen)
    {
      f.print("K;");
    }
    else
    {
      f.print("G;");
    }
    f.print(dateStr + ' ' + timeStr + ';');
    f.println(getUid());
    f.close();
  }
  

  
  digitalWrite(D0, LOW);       
  delay(100);
  digitalWrite(D0, HIGH);  
  delay(100);     
  digitalWrite(D0, LOW);       
  delay(100);
  digitalWrite(D0, HIGH);  

  delay(3000);

}

String twoDig(int val)
{
  String result;
  if (val < 10)
  {
    result = (String)"0" + val;
  }
  else
  {
    result = (String)val;
  }
  return result;
}

void digitalClockDisplay()
{
  if (rtcClock.getSecond() != secondOld)
  {  
    DateTime dt = RTClib::now();
    String dateStr = (String)dt.year() + '-' + twoDig(dt.month()) + '-' + twoDig(dt.day());
    String timeStr = twoDig(dt.hour()) + ':' + twoDig(dt.minute()) + ':' + twoDig(dt.second());

    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.drawString(64, 5, dateStr);
    display.setFont(ArialMT_Plain_24);
    display.drawString(64, 22, timeStr);
    display.display();
  
    secondOld = rtcClock.getSecond();
  }
}





