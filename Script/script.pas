﻿procedure Hauptfenster_Button2_OnClick (Sender: string; var Cancel: boolean);
begin


end;

procedure Form1_Button3_OnClick (Sender: string; var Cancel: boolean);
var
    seconds : integer;
begin
    seconds := SecondsBetween(now(), StrToDateTime('01.01.1970 00:00:00'));
    HTTPGetFile('http://192.168.4.1/time?clk=' + IntToStr(seconds), 'response.txt');
end;


procedure Form1_Button1_OnClick (Sender: string; var Cancel: boolean);
var
   sl : TStringList;
   arrStr: array of string;
   i : integer;
   line, kommen, gehen, letzterTyp : string;
   maListe, kgListe : TDataset;
   maId, maxId, zeilenAnzahl : integer;
   hasKommen : boolean;

begin
    DeleteFile('csvData.txt');
    SQLExecute ('DELETE FROM Input');

    Form1.BtnAnzeigen.Click;

    HTTPGetFile('http://192.168.4.1', 'csvData.txt');

    sl := TStringList.Create;
    sl.LoadFromFile('csvData.txt');

    if (sl[0] = 'Keine Daten !!') then
    begin
        ShowMessage('Keine Daten !!');
        Exit;
    end;

    i := 0;
    while i < sl.Count do
    begin
       line := sl[i];
       arrStr := SplitString(line, ';');
       SQLExecute ('INSERT INTO Input (KG, Zeit, Token) VALUES ("' + arrStr[0] + '",DATETIME("'+ arrStr[1] +'"),"' + arrStr[2] + '");');
       i := i + 1;
    end;

    SQLQuery('SELECT id FROM Mitarbeiter', maListe);

    while (not maListe.EOF) do
    begin
        maId := maListe.FieldByName('id').AsInteger;

        SQLQuery('SELECT KG, Zeit FROM Input WHERE Input.Token IN (SELECT Token FROM Tokens WHERE id_Mitarbeiter=' + IntToStr(maId) + ') ORDER BY Zeit', kgListe);
        while (not kgListe.EOF) do
        begin
            zeilenAnzahl := SQLExecute('SELECT COUNT(id) FROM Zeiterfassung WHERE id_Mitarbeiter=' + IntToStr(maId));

            maxId := 0;
            letzterTyp := 'AU';

            if zeilenAnzahl <> 0 then
            begin
                maxId := SQLExecute('SELECT MAX(id) FROM Zeiterfassung WHERE id_Mitarbeiter=' + IntToStr(maId));
                letzterTyp := SQLExecute('SELECT Typ FROM Zeiterfassung WHERE id=' + IntToStr(maxId));
            end;

            if kgListe.FieldByName('KG').AsString = 'K' then
            begin
                kommen := kgListe.FieldByName('Zeit').AsString;
                SQLExecute ('INSERT INTO Zeiterfassung (Typ, id_Mitarbeiter, Kommt, Geht) VALUES ("K",' + IntToStr(maId) + ',DATETIME("'+ kommen +'"),DATETIME("'+ kommen +'"))' );
            end;

            if kgListe.FieldByName('KG').AsString = 'G' then
            begin
                gehen := kgListe.FieldByName('Zeit').AsString;

                if letzterTyp = 'K' then
                begin
                    SQLExecute ('UPDATE Zeiterfassung SET Typ="KG", Geht = DATETIME("'+ gehen +'") WHERE id=' + IntToStr(maxId));
                end
                else
                begin
                    SQLExecute ('INSERT INTO Zeiterfassung (Typ, id_Mitarbeiter, Kommt, Geht) VALUES ("G",' + IntToStr(maId) + ',DATETIME("'+ gehen +'"),DATETIME("'+ gehen +'"))' );
                end;

            end;

            kgListe.Next;
        end;
        maListe.Next;
    end;


    Form1.BtnAnzeigen.Click;

    //HTTPGetFile('http://192.168.4.1/delete', 'response.txt');

end;

begin
end.                                                                          
